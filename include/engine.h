#pragma once

// lib includes
#include <stack>

// dependencie includes
#include "libtcod.hpp"

// project includes
#include "scene.h"

class Engine {
	
	std::stack<Scene::BaseScene> scenes;

	int h;
	int w;

public:
	Engine();
	~Engine();

	// run
	void run();
	void render();

	//input / render current scene

	

};

