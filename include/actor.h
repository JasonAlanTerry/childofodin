#pragma once

#include <string>

class Actor {

private:
	std::string fname;
	std::string lname;
	std::string birthday;
	
	unsigned int age;
	unsigned int ca;

	unsigned int prowess;
	unsigned int morale;

	unsigned int disc;
	unsigned int stam;
	unsigned int agil;
	unsigned int aggr;
	unsigned int reso;

	unsigned int tempWnds;
	unsigned int permWnds;

public:

	Actor();

	~Actor();

};

