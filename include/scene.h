#pragma once
namespace Scene {

	class BaseScene {

	public:

		BaseScene();
		~BaseScene();

		virtual void input() = 0;
		virtual void update() = 0;

	};

}